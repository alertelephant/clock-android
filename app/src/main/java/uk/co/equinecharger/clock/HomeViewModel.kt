package uk.co.equinecharger.clock

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.json.JSONException

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    val temperature: MutableLiveData<Double> by lazy {
        MutableLiveData<Double>()
    }

    val pressure: MutableLiveData<Double> by lazy {
        MutableLiveData<Double>()
    }

    val humidity: MutableLiveData<Double> by lazy {
        MutableLiveData<Double>()
    }

    val queue = Volley.newRequestQueue(application)

    fun pollClockState(context: Context){

        // We use polling since the clock API does not support web sockets.
        viewModelScope.launch(Dispatchers.IO) {
            while(isActive){
                // Use volley to call to the clock and request the state.
                val jsonRequest = JsonObjectRequest(Request.Method.GET,
                    "http://clock.local/state/",
                    null,
                    {
                        if(it.has("data"))
                        {
                            val data = it.getJSONObject("data")
                            temperature.value = data.getDouble("temperature_clock")
                            pressure.value = data.getDouble("pressure_clock")
                            humidity.value = data.getDouble("humidity_clock")
                        }
                    },
                    {
                        Log.d("CLOCK", "Error in getting state : $it")
                    }
                    )
                queue.add(jsonRequest)

                delay(10000)

            }
        }
    }
}
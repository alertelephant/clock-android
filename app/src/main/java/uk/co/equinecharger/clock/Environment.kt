package uk.co.equinecharger.clock

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.skydoves.colorpickerview.listeners.ColorListener
import kotlinx.android.synthetic.main.clock_fragment.*
import kotlinx.android.synthetic.main.environment_fragment.*

class Environment : Fragment() {

    companion object {
        fun newInstance() = Environment()
    }

    private val environmentViewModel: EnvironmentViewModel by viewModels()
    private var lastColor = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.environment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Wire up our color picker listener via an anon class.
        environmentColorPickerView.colorListener = (object: ColorListener {
            override fun onColorSelected(color: Int, fromUser: Boolean) {
                /**
                 * Color has been selected, get RGB for it and update the singleton.
                 */

                if(lastColor == color) return

                lastColor = color

                // Update settings singleton
                ClockSettings.environment_color = argbToTriple(color)

                // Fire off a commit to the clock, we need the fragments context and the view model
                // to perform this in background (via a corouteen).
                context?.let {
                    ClockSettings.updateToClock(environmentViewModel, it)
                }
            }
        })

        ClockSettings.environment_color?.let{
            /**
             * Set the color picker to the current color of the environment, as reported by the
             * clock.
             */
            environmentColorPickerView.setInitialColor(tripleToArgb(it))
        }

    }
}
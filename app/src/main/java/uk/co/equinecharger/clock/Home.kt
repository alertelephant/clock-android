package uk.co.equinecharger.clock

import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.home_fragment.*
import kotlin.math.round
import kotlin.math.roundToLong

class Home : Fragment() {

    companion object {
        fun newInstance() = Home()
    }

    private val homeViewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.context?.let{ context ->

            // Disable the clock settings buttons.
            clockImageView.isEnabled = false
            newsImageView.isEnabled = false
            lightImageView.isEnabled = false
            environmentImageView.isEnabled = false

            // Color them opaque
            clockImageView.alpha = 0.5F
            newsImageView.alpha = 0.5F
            lightImageView.alpha = 0.5F
            environmentImageView.alpha = 0.5F

            val preferences = PreferenceManager.getDefaultSharedPreferences(context)

            // Setup settings callbacks
            ClockSettings.callbackSettingsLoaded = {
                // Enable clock settings buttons.
                clockImageView.isEnabled = true
                newsImageView.isEnabled = true
                lightImageView.isEnabled = true
                environmentImageView.isEnabled = true

                // Restore alpha
                clockImageView.alpha = 1F
                newsImageView.alpha = 1F
                lightImageView.alpha = 1F
                environmentImageView.alpha = 1F
            }

            ClockSettings.callbackSettingsFailed = {
                // Oh dear!
                // For the time being we will use a toast
                val toasty = Toast.makeText(context, "The settings failed to load, check you " +
                        "are on the house WiFi.",
                    Toast.LENGTH_LONG)
                toasty.show()
            }

            // Load the clock settings.
            ClockSettings.getClockSettings(homeViewModel, context)
            // Set off the polling
            homeViewModel.pollClockState(context)

            // Setup view model listeners so we can update our UI
            // We need access to the view lifrecycle owner.
            homeViewModel.temperature.observe(viewLifecycleOwner) { centigrade ->
                // Depending on the settings we convert to Fahrenheit.
                if(preferences.getBoolean(getString(R.string.fahrenheit_preference),false)){
                    // Convert to fahrenheit and display
                    temperatureView.text = String.format(
                        getString(R.string.fahrenheit), ((centigrade*9/5)+32).round(2))
                } else {
                    temperatureView.text = String.format(
                        getString(R.string.centigrade), centigrade)
                }
            }

            homeViewModel.pressure.observe(viewLifecycleOwner) { pressureHpa ->

                when {
                    preferences.getBoolean(getString(R.string.hectopascal_preference), false) -> {
                        pressureView.text = String.format(getString(R.string.hectopascal),
                            pressureHpa)
                    }
                    // To convert to Inch/Hg and PSI we need the pressure in Pascals, hence the
                    // * 100
                    preferences.getBoolean(getString(R.string.mercury_preference), false) -> {
                        pressureView.text = String.format(getString(R.string.mercury),
                            (pressureHpa*100/3386).round(2) )
                    }
                    preferences.getBoolean(getString(R.string.pounds_per_sq_inch_preference), false) -> {
                        pressureView.text = String.format(getString(R.string.pounds_per_sq_inch),
                            (pressureHpa*100/6895).round(2) )
                    }
                    else -> {
                        // Default to metric hPa
                        pressureView.text = String.format(getString(R.string.hectopascal),
                            pressureHpa)
                    }
                }

            }

            homeViewModel.humidity.observe(viewLifecycleOwner) { humidity ->
                humidityView.text = String.format(getString(R.string.humidity_display), humidity)
            }

        }
    }
}
package uk.co.equinecharger.clock

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.viewModels
import kotlinx.android.synthetic.main.light_fragment.*

class Light : Fragment() {

    companion object {
        fun newInstance() = Light()
    }

    private val lightViewModel: LightViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.light_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup seekbars, range is an Int.
        seekBarBrighness.max = 100
        seekBarBrighness.min = 0
        seekBarDarkBrightness.max= 100
        seekBarDarkBrightness.min = 0

        // Setup listeners on the model view
        lightViewModel.brightness.observe(viewLifecycleOwner) {
            // Update the brightness seek bar
            seekBarBrighness.progress = it
            Log.d("BRIGHT", "Progress brightness = $it")
        }

        lightViewModel.darkBrightness.observe(viewLifecycleOwner) {
            // Update the brightness seek bar
            seekBarDarkBrightness.progress = it
            Log.d("BRIGHT", "Progress dark-brightness = $it")
        }

        // Setup seek bar change listeners
        seekBarBrighness.setOnSeekBarChangeListener(
            (object: SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    seekBar?.let{
                        ClockSettings.brightness = it.progress.toDouble() / 100
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                    // Not implemented
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    /**
                     * Send data to clock.
                     */
                    context?.let{
                        ClockSettings.updateToClock(lightViewModel, it)
                    }
                }

            })
        )

        seekBarDarkBrightness.setOnSeekBarChangeListener(
            (object: SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    seekBar?.let{
                        ClockSettings.dark_brightness = it.progress.toDouble() / 100
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                    // Not implemented
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    /**
                     * Send data to clock.
                     */
                    context?.let{
                        ClockSettings.updateToClock(lightViewModel, it)
                    }
                }

            })
        )

    }

}
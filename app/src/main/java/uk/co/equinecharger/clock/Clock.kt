package uk.co.equinecharger.clock

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.skydoves.colorpickerview.listeners.ColorListener
import kotlinx.android.synthetic.main.clock_fragment.*

class Clock : Fragment() {

    companion object {
        fun newInstance() = Clock()
    }

    private val clockViewModel: ClockViewModel by viewModels()
    private var lastColor: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.clock_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Wire up our color picker listener via an anon class.
        clockTimeColorPickerView.colorListener = (object: ColorListener {
            override fun onColorSelected(color: Int, fromUser: Boolean) {
                /**
                 * Color has been selected, get RGB for it and update the singleton.
                 */

                if(lastColor == color) return

                lastColor = color

                // Update settings singleton
                ClockSettings.time_color = argbToTriple(color)

                // Fire off a commit to the clock, we need the fragments context and the view model
                // to perform this in background (via a corouteen).
                context?.let {
                    ClockSettings.updateToClock(clockViewModel, it)
                }
            }

        })

        ClockSettings.time_color?.let{
            /**
             * Set the color picker to the current color of teh time, as reported by the clock.
             */
            clockTimeColorPickerView.setInitialColor(tripleToArgb(it))
        }
    }
}
package uk.co.equinecharger.clock

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.news_settings_add_schedule_fragment.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.clock_toolbar))

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        /**
         * This is called when a menu is required, this is where your menu is inflated and attached
         * to the action bar.
         */
        // Yes, Google docs don't mention this, took me a while to figure out why my tool bar had
        // no options menu!
        menuInflater.inflate(R.menu.clock_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId){
            R.id.applicationSettingsOption -> {
                // This is an issue, we don't know where we are, so we don't know what path to take.
                // Therefor we have to directly navigate to the settings fragment and NOT use any
                // navigation path to it (in fact no navigation paths exist anyway).
                // Example, navigating via R.id.action_home_to_clock_settings is OK if the home
                // fragment is where you are navigating from.
                // But if you are on another fragment, it makes no sense!
                this.navigateToFragment(R.id.application_settings)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    @Suppress("UNUSED_PARAMETER")
    fun clockSettingsOnClick(view: View) {
        this.navigateToFragment(R.id.action_home_to_clock_settings)
    }

    @Suppress("UNUSED_PARAMETER")
    fun newsSettingsOnClick(view: View) {
        this.navigateToFragment(R.id.action_home_to_news_settings)
    }

    @Suppress("UNUSED_PARAMETER")
    fun lightSettingsOnClick(view: View) {
        this.navigateToFragment(R.id.action_home_to_light_settings)
    }

    @Suppress("UNUSED_PARAMETER")
    fun environmentSettingsOnClick(view: View) {
        this.navigateToFragment(R.id.action_home_to_environment_settings)
    }

    @Suppress("UNUSED_PARAMETER")
    fun newsScheduleOnClick(view: View){
        this.navigateToFragment(R.id.action_news_settings_to_news_settings_schedules)
    }

    @Suppress("UNUSED_PARAMETER")
    fun newsSettingsAddScheduleOnClick(view: View){
        this.navigateToFragment(R.id.action_news_settings_schedules_to_newsSettingsAddSchedule)
    }

    fun newsScheduleAddClick(view: View){
        val start = Pair(scheduleTimePicker.hour, scheduleTimePicker.minute)
        // Work out end time, it may transition midnight.
        val end = if(start.second + scheduleDurationPicker.value > 59){
            var endHour = start.first + 1
            if(endHour > 24) endHour = 0
            Pair(endHour, 59 - (start.second + scheduleDurationPicker.value))
        } else {
            Pair(start.first, start.second+scheduleDurationPicker.value)
        }
        // Add in the schedule to the singleton
        ClockSettings.news_time_segments.add(
            Pair(
                start,
                end
            )
        )

        // We don't notify teh adaptor on data change since the adaptor is re-loaded on the
        // news schedule activity reload.
        // Also we can't anyway since we don't have access to it (it would need to be done via
        // a view model).

        // Navigate back to the news schedule settings fragment.
        // This time we call on onBackPressed to "press" the back button.
        // This will take us back to the news settings chedule activity.
        onBackPressed()
    }
}
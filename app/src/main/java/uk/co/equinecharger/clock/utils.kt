package uk.co.equinecharger.clock

/**
        Utility module
 **/

import android.app.Activity
import android.app.Application
import android.content.Context
import android.util.Log
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.round

infix fun Activity.navigateToFragment(navigableResourceID:Int){
        /**
         * Convenience infix to shorten the code for navigation, from an activity.
         * @param navigableResourceID: A navigation path resource or navigation fragment id.
         */
        // This is subtly different to the normal call of Activity.findNavController(), since
        // we are navigating from an Activity to a Fragment and need to specify the host fragment
        findNavController(this, R.id.fragment_home).navigate(navigableResourceID)
}

infix fun Fragment.navigateToFragment(navigableResourceID:Int){
        /**
         * Convenience infix to shorten the code for navigation, from a fragment.
         * @param navigableResourceID: A navigation path resource or navigation fragment id.
         */
        this.findNavController().navigate(navigableResourceID)
}

object ClockSettings{
        /*
        Singleton that holds the settings, downloaded from the clock
         */
        // We will null out all the settings so we only act on settings received.
        // All set values will be clamped to valid ranges, to prevent bad data getting up stream.

        var callbackSettingsLoaded: (() -> Unit)? = null
        var callbackSettingsFailed: (() -> Unit)? = null

        // Brightness of the display when environment is lit, range is float, 0 to 1
        var brightness: Double? = null
                set(value) {
                        // Clamp between 0 and +1, value may be null
                        field = value?.let{
                                value.coerceIn(0.0, 1.0)
                        }
                }

        // The brightness settings for a dark environment, range is float 0 to 1
        var dark_brightness: Double? = null
                set(value) {
                        // Clamp between 0 and +1, value may be null
                        field = value?.let{
                                value.coerceIn(0.0, 1.0)
                        }
                }

        // The color for the environment data, IE temprature, pressure, humidity, shown in the lower
        // part of the clock matrix display.
        // This is a Triple of R,G,B in the range 0 to 255 for each component.
        var environment_color: Triple<Int, Int, Int>? = null
                set(value) {
                        field = value?.let{
                                val first = it.first.coerceIn(0, 255)
                                val second = it.second.coerceIn(0, 255)
                                val third = it.third.coerceIn(0, 255)
                                Triple(first, second, third)
                        }
                }

        // If true then the clock is displaying the external environment provided by the GDS.
        var environment_outside: Boolean? = null

        // Advanced setting, this is the lux upper range value, lower range is always 0.
        // This represents the maximum value from the light sensor, that represents "daylight".
        var lux_range: Double? = null
                set(value) {
                        field = value?.let {
                                // The typical value for "daylight" is 3.5, the API currently allows
                                // any value to be set, so we will clamp to reasonable value of 4.0
                                value.coerceIn(0.0, 4.0)
                        }

                }
        // The delay in seconds and fractional seconds before updating the marquee animation.
        // The delay will be approximate, since the clock code uses coroutines.
        var marquee_scroll_delay: Double? = null
                set(value){
                        field = value?.let{
                                // This can be set to any value, but we want to clamp to a
                                // reasonable range.
                                value.coerceIn(0.05,2.0)
                        }
                }

        var marquee_scroll_distance: Int? = null
                set(value){
                        field = value?.let{
                                // This can be set to any value, a reasonable range would be 1 to
                                // 10
                                value.coerceIn(1,10)
                        }
                }


        // If set true this will show the news all the time, at the cost of the environment display.
        var news_always: Boolean? = null

        // If set the news is never shown, this overrides news_always above.
        var news_never: Boolean? = null

        // This one is complex, it is a list of Pairs of Pairs.
        // Each pair in the list is a pair of time schedules, start time and end time.
        // Time is in local time.
        var news_time_segments: MutableList<Pair<Pair<Int,Int>,Pair<Int,Int>>> = mutableListOf()

        // The color of the news ticker.
        var news_color: Triple<Int, Int, Int>? = null
                set(value) {
                        field = value?.let{
                                val first = it.first.coerceIn(0, 255)
                                val second = it.second.coerceIn(0, 255)
                                val third = it.third.coerceIn(0, 255)
                                Triple(first, second, third)
                        }
                }

        // This is used as a re-load flag that is set when you want to flag a change to the
        // news visual settings (IE the marquee).
        // This is recognised on the clock side API and will result in the text being re-loaded.
        // This saves on us jumping through odd loops and having to utilise timeouts.
        var news_reload = false

        // The color of the time.
        var time_color: Triple<Int, Int, Int>? = null
                set(value) {
                        field = value?.let{
                                val first = it.first.coerceIn(0, 255)
                                val second = it.second.coerceIn(0, 255)
                                val third = it.third.coerceIn(0, 255)
                                Triple(first, second, third)
                        }
                }

        // Volley queue
        var queue: RequestQueue? = null

        fun decodeColor(color: JSONArray):Triple<Int, Int, Int>{
                /**
                 * Decode the color data provided by the clock into our application representation.
                 */
                return Triple(color[0] as Int, color[1] as Int, color[2] as Int)
        }

        fun updateToClock(viewModel: ViewModel, context: Context){
                /**
                 * Send settings back to the clock.
                 */

                // Convert settings into data payload for posting up to the clock
                // We will do this manually instead of using libraries like Gjson, since we know
                // the format of the data.

                // Generate a JSON serializable data object which will be a map.
                val jsonMap = mutableMapOf<String, Any>()

                // Note that all of the variables are nullables, the clock API handles nullables
                // as "missing" and they are left unchanged.
                brightness?.let{
                        jsonMap["brightness"] = it
                }

                dark_brightness?.let{
                        jsonMap["dark_brightness"] = it
                }

                environment_color?.let{
                        jsonMap["environment_color"] = listOf<Int>(
                                it.first, it.second, it.third
                        )
                }

                environment_outside?.let{
                        jsonMap["environment_outside"] = it
                }

                lux_range?.let{
                        jsonMap["lux_range"] = it
                }

                marquee_scroll_delay?.let{
                        jsonMap["marquee_scroll_delay"] = it
                }

                marquee_scroll_distance?.let{
                        jsonMap["marquee_scroll_distance"] = it
                }

                news_always?.let{
                        jsonMap["news_always"] = it
                }

                news_never?.let{
                        jsonMap["news_never"] = it
                }

                jsonMap["news_reload"] = news_reload

                news_time_segments.let{
                        // todo: process these into pairs of [[start time, end time], ]
                        //jsonMap["news_time_segments"] = it
                        val segmentsAsList = mutableListOf<List<List<Int>>>()
                        for(timeSegment in it){
                                // Extract start and end time pairs
                                val start = timeSegment.first
                                val end = timeSegment.second
                                // Convert to nested list.
                                segmentsAsList.add(listOf(
                                        listOf(start.first, start.second),
                                        listOf(end.first, end.second)
                                        )
                                )
                        }
                        jsonMap["news_time_segments"] = segmentsAsList
                }

                news_color?.let {
                        jsonMap["news_color"] = listOf<Int>(
                                it.first, it.second, it.third
                        )
                }

                time_color?.let {
                        jsonMap["time_color"] = listOf<Int>(
                                it.first, it.second, it.third
                        )
                }

                // Convert our object to JSON object
                val jsonObject = JSONObject(jsonMap as Map<*, *>)

                // Via a corouteen send the data off to the clock
                viewModel.viewModelScope.launch {

                        val jsonRequest = JsonObjectRequest(
                                Request.Method.POST,
                                "http://clock.local/settings/update/",
                                jsonObject,
                                {
                                        Log.d("CLOCK-SETTINGS", "Response $it")
                                },
                                {
                                        Log.d("CLOCK-SETTINGS", "Error in updating settings : $it")
                                }
                        )

                        if(queue == null){
                                queue = Volley.newRequestQueue(context)
                        }

                        queue?.add(jsonRequest)
                }
        }

        fun getClockSettings(viewModel: ViewModel, context: Context){
                /**
                 * Fetch the clock settings and update the settings singleton.
                 */
                viewModel.viewModelScope.launch(Dispatchers.IO) {

                        val jsonRequest = JsonObjectRequest(
                                Request.Method.GET,
                                "http://clock.local/settings/", null, {
                                        Log.d("CLOCK-SETTINGS", "Data:: $it")
                                        if(it.has("data")){
                                                // Load in the settings
                                                val settings = it.getJSONObject("data")

                                                brightness = settings.getDouble("brightness")
                                                dark_brightness = settings.getDouble("dark_brightness")
                                                environment_color = decodeColor(
                                                        settings.getJSONArray("environment_color"))
                                                environment_outside = settings.getBoolean(
                                                        "environment_outside")
                                                lux_range = settings.getDouble("lux_range")
                                                marquee_scroll_delay = settings.getDouble(
                                                        "marquee_scroll_delay")
                                                marquee_scroll_distance = settings.getInt(
                                                        "marquee_scroll_distance")
                                                news_always = settings.getBoolean("news_always")
                                                news_never = settings.getBoolean("news_never")
                                                news_color = decodeColor(
                                                        settings.getJSONArray("news_color"))
                                                time_color = decodeColor(
                                                        settings.getJSONArray("time_color")

                                                )

                                                // News time segments (news schedules) this is a
                                                // complex array list of the form:
                                                // [ [ [start hour, start minute], [end hour, end minute]], ... ]
                                                // which needs to be converted into a list of Pair
                                                // objects
                                                // Unfortunately JSONArray did not have an iterator,
                                                // and you cannot patch one in via infix - pity.
                                                val schedules = settings.getJSONArray(
                                                        "news_time_segments")

                                                news_time_segments = mutableListOf()

                                                for(index in 0 until schedules.length()){
                                                        val schedule = schedules.get(index) as JSONArray
                                                        val start = schedule.getJSONArray(0)
                                                        val end = schedule.getJSONArray(1)
                                                        // Composit into our internal format and
                                                        // and add to our schedule list.
                                                        news_time_segments.add(Pair(
                                                                Pair(start[0] as Int, start[1] as Int),
                                                                Pair(end[0] as Int, end[1] as Int)
                                                        ))
                                                }

                                                // Call settings loaded callback
                                                callbackSettingsLoaded?.invoke()

                                        } else {
                                                // Something is not right call the settings failed
                                                // callback
                                                callbackSettingsFailed?.invoke()
                                        }


                                }, {
                                        Log.d("CLOCK-SETTINGS",
                                                "Error in getting settings : $it")
                                        // Settings failed!
                                        callbackSettingsFailed?.invoke()
                                })

                        if(queue == null){
                                queue = Volley.newRequestQueue(context)
                        }

                        queue?.add(jsonRequest)

                }
        }
}

fun tripleToArgb(color: Triple<Int, Int, Int>): Int{
        /**
         * Convert our triple color representation to Argb.
         */
        // How we would like to do this:
        // val Argb = Color(color.first, color.second, color.third).toArgb()
        // Unfortunately due to this strange Kotlin/IntelliJ bug:
        // https://youtrack.jetbrains.com/issue/KT-44886
        // We can't!
        return (255 shl 24) or (color.first shl 16) or (color.second shl 8) or color.third
}

fun argbToTriple(color: Int): Triple<Int, Int, Int>{
        /**
         * Convert a packed Color Int object back into a triple
         */
        val r = (color shr 16) and 0xFF
        val g = (color shr 8) and 0xFF
        val b = color and 0xFF
        return Triple(r,g,b)
}

// Patch Double to do rounding to specific DPs
fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        repeat(decimals) { multiplier *= 10 }
        return round(this * multiplier) / multiplier
}

data class Time(internal var hours: Int, internal var minutes: Int, internal var seconds: Int){
        /**
         * There is no library in Java or Kotlin for basic time maths!
         */

        operator fun minus(stop: Time): Time{
                val start = this
                val diff = Time(0, 0, 0)

                if (stop.seconds > start.seconds) {
                        --this.minutes
                        this.seconds += 60
                }

                diff.seconds = this.seconds - stop.seconds
                if (stop.minutes > start.minutes) {
                        --this.hours
                        this.minutes += 60
                }

                diff.minutes = this.minutes - stop.minutes
                diff.hours = this.hours - stop.hours

                return diff
        }
}
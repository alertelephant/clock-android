package uk.co.equinecharger.clock

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LightViewModel : ViewModel() {
    val brightness: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also {
            ClockSettings.brightness?.let{ brightness ->
                it.value = (brightness * 100).toInt().coerceIn(0, 100)
            }
        }
    }

    val darkBrightness: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also {
            ClockSettings.dark_brightness?.let{ dark_brightness ->
                it.value = (dark_brightness * 100).toInt().coerceIn(0, 100)
            }
        }
    }

    private fun loadBrightness(){
        ClockSettings.brightness?.let{
            brightness.value = (it * 100).toInt().coerceIn(0, 100)
        }
    }
}
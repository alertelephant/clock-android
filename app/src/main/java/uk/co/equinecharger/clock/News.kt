package uk.co.equinecharger.clock

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleAdapter
import androidx.fragment.app.viewModels
import com.skydoves.colorpickerview.listeners.ColorListener
import kotlinx.android.synthetic.main.clock_fragment.*
import kotlinx.android.synthetic.main.news_fragment.*
import kotlinx.android.synthetic.main.news_settings_schedules_fragment.*
import kotlin.properties.Delegates

class News : Fragment() {

    companion object {
        fun newInstance() = News()
    }

    private val newsViewModel: NewsViewModel by viewModels()
    private var lastColor = 0

    // The initial news always and never state flags from settings.
    // if this has value on this fragments demise, then the clock settings will be
    // reverted back to these values.
    private var initialNewsAlways:Boolean? = null
    private var initialNewsNever:Boolean? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialNewsAlways = ClockSettings.news_always
        initialNewsNever = ClockSettings.news_never

        // Wire up our color picker listener via an anon class.
        newsMarqueeColorPickerView.colorListener = (object: ColorListener {
            override fun onColorSelected(color: Int, fromUser: Boolean) {
                /**
                 * Color has been selected, get RGB for it and update the singleton.
                 */

                if(lastColor == color) return

                lastColor = color

                // Update settings singleton
                ClockSettings.news_color = argbToTriple(color)

                // Fire off a commit to the clock, we need the fragments context and the view model
                // to perform this in background (via a corouteen).
                context?.let {
                    // Request on the update a news reload.
                    ClockSettings.news_reload = true
                    // We need to switch off then on the news, to show the change
                    // But we need to delay to allow the clock API to register this
                    ClockSettings.news_always = true
                    ClockSettings.news_never = false
                    ClockSettings.updateToClock(newsViewModel, it)
                }
            }
        })

        ClockSettings.news_color?.let{
            /**
             * Set the color picker to the current color of the news, as reported by the clock.
             */
            newsMarqueeColorPickerView.setInitialColor(tripleToArgb(it))
        }

        // Wire up the news show and hide toggle switches
        newsShowAlwaysSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            // Set news hidden switch off
            newsHideAlwaysSwitch.isChecked = if(isChecked) false else newsHideAlwaysSwitch.isChecked
            ClockSettings.news_always = isChecked
            this.context?.let {
                ClockSettings.updateToClock(newsViewModel, it)
            }
            initialNewsAlways = null
            initialNewsNever = null
        }

        newsHideAlwaysSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            // Set news always switch off
            newsShowAlwaysSwitch.isChecked = if(isChecked) false else newsShowAlwaysSwitch.isChecked
            ClockSettings.news_never = isChecked
            this.context?.let {
                ClockSettings.updateToClock(newsViewModel, it)
            }
            initialNewsAlways = null
            initialNewsNever = null
        }

    }

    override fun onPause() {
        /**
         * Restore the initial news settings if they exist.
         */
        restoreSettings()
        super.onPause()
    }

    override fun onResume() {
        /**
         * Resume news setup
         */

        ClockSettings.news_always=true
        ClockSettings.news_reload=true
        this.context?.let {
            ClockSettings.updateToClock(newsViewModel, it)
        }
        super.onResume()
    }

    private fun restoreSettings(){
        /**
         * Restore initial news settings.
         */
        initialNewsNever?.let{
            ClockSettings.news_never = it
        }
        initialNewsAlways?.let{
            ClockSettings.news_always = it
        }
        ClockSettings.news_reload = false
        this.context?.let{
            ClockSettings.updateToClock(newsViewModel, it)
        }
    }

}
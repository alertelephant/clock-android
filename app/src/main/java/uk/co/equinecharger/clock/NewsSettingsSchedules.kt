package uk.co.equinecharger.clock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.news_settings_schedules_fragment.*


class NewsSettingsSchedules : Fragment() {

    companion object {
        fun newInstance() = NewsSettingsSchedules()
    }

    private val newsSettingsViewModel: NewsSettingsSchedulesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.news_settings_schedules_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Link up our adapter
        newsScheduleRecyclerView.adapter = NewsScheduleAdaptor()
        // Attach our layout manager
        newsScheduleRecyclerView.layoutManager = LinearLayoutManager(this.context)

        val mIth = ItemTouchHelper(
            object : ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.RIGHT,
                ItemTouchHelper.LEFT
            ) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: ViewHolder, target: ViewHolder
                ): Boolean {
                    val fromPos = viewHolder.adapterPosition
                    val toPos = target.adapterPosition
                    // We don't care for position movement in adaptor, direction will be left/right
                    // only, not up/down.
                    return fromPos != toPos // true // true if moved, false otherwise
                }

                override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
                    // remove from adapter viewHolder.layoutPosition has the position
                    val position = viewHolder.layoutPosition

                    newsScheduleRecyclerView.adapter?.let{
                        ClockSettings.news_time_segments.removeAt(position)
                        it.notifyItemRemoved(position)
                    }
                }
            })

        mIth.attachToRecyclerView(newsScheduleRecyclerView)
    }

}
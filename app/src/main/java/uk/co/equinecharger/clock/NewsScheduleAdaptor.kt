package uk.co.equinecharger.clock

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class NewsScheduleAdaptor: RecyclerView.Adapter<NewsScheduleAdaptor.NewsScheduleViewHolder> () {
    /**
     * Recycler class adapter.
     * This does not need to have the list of schedules passed over, since they already exist in
     * the clock settings singleton.
     */

    class NewsScheduleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val startText = itemView.findViewById<TextView>(R.id.textViewStartTime)
        val durationText = itemView.findViewById<TextView>(R.id.textViewDuration)
        val startTimeText = itemView.findViewById<TextView>(R.id.startTimeTextView)
        val durationTimeText = itemView.findViewById<TextView>(R.id.durationTextView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsScheduleViewHolder {
        // Inflate our view holder layout for a news schedule.
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.news_schedule_recycler_view, parent, false)
        // Associate the view with our view holder and return.
        return NewsScheduleViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsScheduleViewHolder, position: Int) {
        // Get our schedule but only if we have data.
        ClockSettings.news_time_segments.also { schedules ->
            // The schedule consists of a Pair (tuple) which we can deconstruct into a start and end
            // time.
            val (startTime, endTime) = schedules[position]
            // This schedule consists of a pair of time values, we need to do time maths to
            // derive a duration.
            // We use our own 'Time' class to subtract end time from start time
            val diff = Time(endTime.first, endTime.second, 0) - Time(startTime.first,
                startTime.second, 0)

            // Bind to our view.
            holder.startText.text = "${startTime.first}:${startTime.second}"
            holder.durationText.text = "${diff.minutes}"

            // if this is position > 0 hide the start time and duration text with "GONE" so it
            // not just only hides, it does not contribute to the layout.
            if(position > 0) {
                holder.startTimeText.visibility = View.GONE
                holder.durationTimeText.visibility = View.GONE
            } else {
                // Reverse the process for item 0
                holder.startTimeText.visibility = View.VISIBLE
                holder.durationTimeText.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int {
        return ClockSettings.news_time_segments.size
    }
}
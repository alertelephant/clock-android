package uk.co.equinecharger.clock

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import kotlinx.android.synthetic.main.news_settings_add_schedule_fragment.*

class NewsSettingsAddSchedule : Fragment() {

    companion object {
        fun newInstance() = NewsSettingsAddSchedule()
    }

    private val newsSettingsAddScheduleViewModel: NewsSettingsAddScheduleViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.news_settings_add_schedule_fragment, container,
            false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Switch time picker to 24 hour
        // The setter appears to have no Kotlin property equivalent.
        scheduleTimePicker.setIs24HourView(true)

        scheduleDurationPicker.minValue = 3
        scheduleDurationPicker.maxValue = 10
        super.onViewCreated(view, savedInstanceState)
    }

}
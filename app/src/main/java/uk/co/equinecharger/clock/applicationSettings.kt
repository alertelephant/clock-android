package uk.co.equinecharger.clock

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference

class applicationSettings : PreferenceFragmentCompat() {
    /**
     * Settings fragment, which takes care of saving the settings on application termination/
     * configuration change.
     */
    companion object {
        fun newInstance() = applicationSettings()
    }

    private val applicationSettingsViewModel: ApplicationSettingsViewModel by viewModels()

// Fragment replaced with preferences API preference fragment.
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        return inflater.inflate(R.layout.application_settings_fragment, container, false)
//    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.settings, rootKey)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        /**
         * Detect which preference has been clicked and un-click diametrically opposed preferences.
         */

        context?.let { context ->
            // To save on calls to getString and findPreference
            val centigrade_preference = findPreference<SwitchPreference>(
                context.getString(R.string.centigrade_preference))
            val fahrenheit_preference = findPreference<SwitchPreference>(
                context.getString(R.string.fahrenheit_preference))
            val hectopascal_preference = findPreference<SwitchPreference>(
                context.getString(R.string.hectopascal_preference))
            val mercury_preference = findPreference<SwitchPreference>(
                context.getString(R.string.mercury_preference))
            val pounds_per_sq_inch_preference = findPreference<SwitchPreference>(context.getString(
                R.string.pounds_per_sq_inch_preference))

            when (preference?.key) {

                centigrade_preference?.key -> {
                    // Flip over the state of the fahrenheit preference to be the opposite of this
                    fahrenheit_preference?.isChecked = !(centigrade_preference?.isChecked ?: true)
                }
                fahrenheit_preference?.key -> {
                    // Flip over the state of the centigrade preference to be the opposite of this
                    centigrade_preference?.isChecked = !(fahrenheit_preference?.isChecked ?: true)
                }

                // Bit more complicated.
                hectopascal_preference?.key -> {
                    // We have three switches in play, the next one is chosen to be th opposite
                    // of this switch.
                    mercury_preference?.isChecked = !(hectopascal_preference?.isChecked ?: true)
                    // The one after that is set off.
                    pounds_per_sq_inch_preference?.isChecked = false
                }

                mercury_preference?.key -> {
                    // We have three switches in play, the next one is chosen to be th opposite
                    // of this switch.
                    pounds_per_sq_inch_preference?.isChecked = !(
                            mercury_preference?.isChecked ?: true)
                    // The one after that is set off.
                    hectopascal_preference?.isChecked = false
                }

                pounds_per_sq_inch_preference?.key -> {
                    // We have three switches in play, the next one is chosen to be th opposite
                    // of this switch.
                    hectopascal_preference?.isChecked = !(
                            pounds_per_sq_inch_preference?.isChecked ?: true)
                    // The one after that is set off.
                    mercury_preference?.isChecked = false
                }

                // There is nothing "else" we can do!
                else -> {}
            }

        }
        return super.onPreferenceTreeClick(preference)
    }

}